#!/usr/bin/env sh
set -e

 wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/jdk-8u131-linux-x64.tar.gz"
tar xzf jdk-8u131-linux-x64.tar.gz

# make sure the oracle jdk is the default
update-alternatives --install /usr/bin/java java /usr/java/latest/bin/java 1065
update-alternatives --install /usr/bin/javac javac /usr/java/latest/bin/javac 1065
update-alternatives --install /usr/bin/jar jar /usr/java/latest/bin/jar 1065
update-alternatives --install /usr/bin/javaws javaws /usr/java/latest/bin/javaws 1065

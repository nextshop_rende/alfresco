package ECM;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.Date;

public class UploadFile extends ConnectUploadFile {

    public static void main(String[] args) {
    }

    public void initLog(String logName) {
        super.initLog(logName);
    }

    public void endLog() {
        super.endLog();
    }

    public void connect(String ambiente) {
        logger.info("Avvio la connessione all'ambiente " + ambiente);
        String url = "";
        try {
            this.getBrowser();
            logger.info("Connessione avvenuta con successo.");
        } catch (Exception e) {
            logger.severe("Si è verificata la seguente eccezione nel corso della connessione " + e.getMessage());
        }
        driver.manage().window().maximize();
        logger.info("Inizio le operazioni di login");
        if ("locale".equals(ambiente)) {
            url = "http://localhost:8080/share/page/";
            driver.get(url);
            driver.findElement(By.id("page_x002e_components_x002e_slingshot-login_x0023_default-username")).sendKeys("admin");
            driver.findElement(By.id("page_x002e_components_x002e_slingshot-login_x0023_default-password")).sendKeys("Alkemy00a");
            driver.findElement(By.id("page_x002e_components_x002e_slingshot-login_x0023_default-submit-button")).click();
            logger.info("Login avvenuto con successo");

        } else if ("test".equals(ambiente)) {
            url = "http:// ";
            driver.get(url);
            driver.findElement(By.id(" ")).sendKeys(" ");
            driver.findElement(By.id(" ")).sendKeys(" ");
            driver.findElement(By.id(" ")).click();
            logger.info("Login avvenuto con successo");

        } else if ("produzione".equals(ambiente)) {
            url = "http://";
            driver.get(url);
            driver.findElement(By.id(" ")).sendKeys(" ");
            driver.findElement(By.id(" ")).sendKeys(" ");
            driver.findElement(By.id(" ")).click();
            driver.findElement(By.id(" ")).click();
            logger.info("Login avvenuto con successo");

        } else {
            if (driver instanceof JavascriptExecutor) {
                ((JavascriptExecutor) driver)
                        .executeScript("alert('Non è possibile proseguire perché l\\'ambiente impostato non esiste');");
            }
            logger.severe("Non è possibile proseguire perché l'ambiente impostato " + ambiente + "non esiste");
            return;
        }
        this.finestraPrincipale = driver.getWindowHandle();
    }

    public void uploadDiUnFile() {

        Date dataInizio = new Date(System.currentTimeMillis());
        System.out.println("\n INIZIO TEST: "+dataInizio+"\n");

        try {
            Thread.sleep(3000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("I miei file")));
        driver.findElement(By.linkText("I miei file")).click();

        try {
            Thread.sleep(3000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("CartellaTest")));
        driver.findElement(By.linkText("CartellaTest")).click();

        try {
            Thread.sleep(5000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("template_x002e_documentlist_v2_x002e_myfiles_x0023_default-fileUpload-button-button")));
        driver.findElement(By.id("template_x002e_documentlist_v2_x002e_myfiles_x0023_default-fileUpload-button-button")).click();

        try {
            Thread.sleep(3000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("files[]")));
        driver.findElement(By.name("files[]")).click();

        try {
            Thread.sleep(3000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name("files[]")));
        driver.findElement(By.name("files[]")).sendKeys("/home/ciullo/Scrivania/documentoDiTest");

        try {
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_ESCAPE);
            robot.keyRelease(KeyEvent.VK_ESCAPE);
        }
        catch (AWTException e)
        {
            e.printStackTrace();
        }

        try {
            Thread.sleep(3000);//1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText("documentoDiTest")));

        if(driver.findElement(By.linkText("documentoDiTest")).isEnabled()){

            System.out.println("L'upload del file È AVVENUTO CON SUCCESSO");

        }else{

            System.out.println("L'upload del file NON È AVVENUTO");
        }


        Date dataFine = new Date(System.currentTimeMillis());
        System.out.println("\n FINE TEST: "+dataFine);

    }

}

package ECM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.interactions.Actions;
import java.util.logging.*;

public class ConnectUploadFile {
    protected WebDriver driver;
    protected String finestraPrincipale = null;


    protected WebDriverWait wait;
    protected WebDriverWait shortwait;
    protected Actions actions;


    protected static final String PATH_TO_CROME = "target/classes/chromedriver/chromedriver"; //proprio path


    protected Logger logger;
    protected FileHandler fh;

    public static void main(String[] args){

    }
    public void initLog(String logName){
        try {
            logger = Logger.getLogger(logName);
            fh = new FileHandler(logName + ".txt");
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
            logger.addHandler(fh);
        } catch (Exception e){
            logger.info("Non è stato possibile creare il file di log");
        }
    }
    public void endLog(){
        fh.close();
    }

    protected void getBrowser(){
        System.setProperty("webdriver.chrome.driver", PATH_TO_CROME);
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10000);
        shortwait = new WebDriverWait(driver, 5);
        actions = new Actions(driver);
    }


}
